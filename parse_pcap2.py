from scapy.all import *
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
import pygal
import nfq
import os
import numpy


class PcapParser(object):

    """PcapParser"""

    def __init__(self, net_config_file):
        self.deltas = []
        self.net_config_file = net_config_file

    def populate_deltas(self):
        packets_recv = rdpcap('./out/tcpdump_recv.dmp')
        self.rtp_packets_recv = []
        for packet in packets_recv:
            if 'UDP' in packet and packet.dport == 1234:
                self.rtp_packets_recv.append(packet)

        packets_sent = rdpcap('./out/tcpdump_sender.dmp')
        self.rtp_packets_sent = []
        for packet in packets_sent:
            if 'UDP' in packet and packet.dport == 1234:
                self.rtp_packets_sent.append(packet)

        self.deltas = []
        for index, rtp_packet in enumerate(self.rtp_packets_recv):
            if index + 1 >= len(self.rtp_packets_recv):
                break

            self.deltas.append(
                round(float((self.rtp_packets_recv[index+1].time - rtp_packet.time) * 1000), 3))

    def calc_end_to_end_delay_from_dumps(self):
        rtp_end_to_end = []

        for packet_sent in self.rtp_packets_sent:
            timestamp = packet_sent.getlayer(Raw).load.encode("HEX")[8:16]
            for packet_recv in self.rtp_packets_recv:
                if timestamp == packet_recv.getlayer(Raw).load.encode("HEX")[8:16]:
                    packet = {
                        'rtp_timestamp': int(timestamp, 16),
                        'time_sent': packet_sent.time,
                        'time_recv': packet_recv.time,
                        'delta': round((packet_recv.time - packet_sent.time) * 1000, 3)
                    }
                    rtp_end_to_end.append(packet)
                    break

        rtp_end_to_end.sort(key=lambda p: p['rtp_timestamp'])

        delays = [packet['delta'] for packet in rtp_end_to_end]
        delays = map(lambda x,y: (x, y), range(len(delays)), delays)
        PcapParser.plot_single_data_set(delays, './out/end_to_end_delay_from_tcpdump_plot')

        with open('./out/end_to_end_delay_from_tcpdump', 'w') as delay_file:
            delay_file.write(str(rtp_end_to_end))

    def calc_end_to_end_delay(self):
        with open('./out/run.log') as log:
            lines = log.readlines()

        arrival_str = '(neteq_impl.cc:154): NetEqImpl::InsertPacket, time: '
        end_to_end_delay_str = '(neteq_impl.cc:874): End to end delay so far: '
        arrivals = []
        end_to_end_delays = []
        for line in lines:
            if arrival_str in line:
                arrivals.append(int(line[len(arrival_str):]))
            elif end_to_end_delay_str in line:
                delay = int(line[len(end_to_end_delay_str):])
                if delay > 0:
                    end_to_end_delays.append(delay)

        self.deltas_from_app = numpy.diff(arrivals)
        self.deltas_from_app = map(lambda x,y: (x, y), range(len(self.deltas_from_app)), self.deltas_from_app)
        end_to_end_delays = map(lambda x,y: (x, y), range(len(end_to_end_delays)), end_to_end_delays)

        with open('./out/delta', 'w') as arrival_file:
            arrival_file.write(str(self.deltas_from_app))

        PcapParser.plot_single_data_set(self.deltas_from_app, './out/delta_plot')

        with open('./out/end_to_end_delay', 'w') as delay_file:
            delay_file.write(str(end_to_end_delays))

        PcapParser.plot_single_data_set(end_to_end_delays, './out/end_to_end_delay_plot')

    @staticmethod
    def plot_single_data_set(data, file_name):
        xy_chart = pygal.XY(stroke=False, x_title='Packet number', y_title=file_name)
        xy_chart.add('Expected', data)
        xy_chart.render_to_file(file_name)
        PcapParser.svgexport(file_name)

    def calc_jitter(self):
        actual_jitter = map(
            lambda x, y: y-x, 
            self.deltas[:-1], 
            self.deltas[1:])

        actual_jitter = map(
            lambda x, y: (x, y), 
            actual_jitter, 
            range(len(actual_jitter)))

        packets = list(nfq.Simulation.load_packet_config(self.net_config_file))

        expected_jitter = map(
            lambda x, y: y-x,
            packets[:-1],
            packets[1:])

        expected_jitter = map(
            lambda x, y: (x+0.12, y),
            expected_jitter,
            range(len(expected_jitter)))

        # expected_jitter = filter(lambda x: abs(x[0]) > 5, expected_jitter)
        # actual_jitter = filter(lambda x: abs(x[0]) > 5, actual_jitter)

        PcapParser.plot(actual_jitter, expected_jitter, file_name='./out/jitter.svg')

    def plot_traces(self):
        actual_trace = map(
            lambda x, y: (x, y),
            range(len(self.deltas)),
            self.deltas
        )

        with open('./out/actual_deltas', 'w') as actual_deltas_file:
            for delta in self.deltas:                
                actual_deltas_file.write(str(delta) + '\n')

        packets = list(nfq.Simulation.load_packet_config(self.net_config_file))
        expected_trace = map(
            lambda x, y: (x, y+0.12),
            range(len(packets)),
            packets
        )

        PcapParser.plot(actual_trace, expected_trace, file_name='./out/trace.svg')

    @staticmethod
    def plot(actual, expected, file_name='./out/jitter.svg'):
        """
        data is a list of (x, y)
        """
        xy_chart = pygal.XY(stroke=False, legend_at_bottom=True, x_title='Packet number', y_title='Delay')
        xy_chart.add('Expected', expected)
        xy_chart.add('Actual', actual)
        
        xy_chart.render_to_file(file_name)

        PcapParser.svgexport(file_name)

    @staticmethod
    def svgexport(file_name):
        command = 'svgexport ' + file_name + ' ' + file_name[:-4] + '.png'
        os.system(command)

    @staticmethod
    def scatterplot(x, y, file_name='./out/jitter.png'):
        pyplot.plot(x, y, 'b.')
        pyplot.xlim(min(x)-1, max(x)+1)
        pyplot.ylim(min(y)-1, max(y)+1)
        pyplot.savefig(file_name)
        # pyplot.show()

    def run(self):
        print 'PcapParser run'
        self.populate_deltas()
        self.calc_end_to_end_delay_from_dumps()
        self.calc_end_to_end_delay()
        PcapParser.scatterplot(xrange(len(self.deltas)), self.deltas)
        self.calc_jitter()
        self.plot_traces()


def main():
    p = PcapParser('./profiles/moha.19')
    p.run()
    return p

if __name__ == '__main__':
    parser = main()
