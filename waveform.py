#!/usr/bin/env python
import sys
from pylab import *
import wave
from glob import glob

def show_wave_n_spec(hybrid=True):

    speech = './out/recorded_mic.pcm.wav'
    spf = wave.open(speech,'r')
    sound_info = spf.readframes(-1)
    sound_info = fromstring(sound_info, 'Int16')
    
    if hybrid: subplot(211)
    else: subplot(311)
    plot(sound_info)
    ylabel('Amplitude')
    title('Waveform from %s' % 'reference file (sender)')

    speech = './out/recorded_playout.pcm.wav'
    spf = wave.open(speech,'r')
    sound_info = spf.readframes(-1)
    sound_info = fromstring(sound_info, 'Int16')
    
    if hybrid: subplot(212)
    else: subplot(312)
    plot(sound_info)
    xlabel('Sample Number')
    ylabel('Amplitude')
    title('Waveform from %s' % 'recorded file (receiver)')
    
    savefig("./out/waveforms.png")
    
    spf.close()


def sample_numbers(speech):
    spf = wave.open(speech,'r')
    sound_info = spf.readframes(-1)
    sound_info = fromstring(sound_info, 'Int16')
    print speech, len(sound_info)
    return len(sound_info) 


def analyse_sample_numbers():
    ref = sample_numbers('./results/1/out_hybrid/recorded_mic.pcm.wav')
    experiments = glob('./results2/*')
    # experiments = experiments[:1]
    for experiment in experiments:
        hybrid_samples = sample_numbers(experiment + '/out_hybrid/recorded_playout.pcm.wav')
        no_hybrid_samples = sample_numbers(experiment + '/out_no_hybrid/recorded_playout.pcm.wav')
        print 'difference_hyb_no_hyb:', str(hybrid_samples - no_hybrid_samples)
        print 'difference_ref_hyb:', str(ref - hybrid_samples)
        print 'difference_ref_no_hyb:', str(ref - no_hybrid_samples)



if __name__ == '__main__':
    analyse_sample_numbers()
    # show_wave_n_spec()
