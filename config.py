import json

class Config(object):

    """Config class for the test"""

    def __init__(self):
        pass
        #with open('config.json') as config:
        #    self.config = json.load(config)

    def get_config(self, key):
        try:
            if key == 'packetConfig':
                return './profiles/moha.19'
            if key == 'duration':
                return 75
            return self.config[key]
        except IndexError:
            print ('no such key: {}'.format(key))
            return None
