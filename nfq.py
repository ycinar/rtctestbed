import parse_pcap2 as parse_pcap
import config

from netfilterqueue import NetfilterQueue
import time
from collections import deque
import logging
import json

LOG_FILENAME = 'jitter.log'
logging.basicConfig(filename=LOG_FILENAME,
                    level=logging.DEBUG,
                    )

class Simulation(object):
    """Simulation Class"""
    @staticmethod
    def load_packet_config(net_config):
        with open(net_config) as packet_config:
            packet_config = packet_config.read()
        packet_config = packet_config.split()

        packet_config_int = []
        for packet in packet_config:
            packet_config_int.append(float(packet))

        return deque(packet_config_int[:4500])

    def __init__(self, net_config):
        self.net_config = net_config
        self.packet_config = Simulation.load_packet_config(net_config)
        parse_pcap.PcapParser(net_config).scatterplot(
            xrange(len(self.packet_config)),
            self.packet_config,
            './out/expected.png')

        packets = list(self.packet_config)
        
        expected_jitter = map(
            lambda x, y: y-x,
            packets[:-1],
            packets[1:])

        expected_jitter = filter(lambda x: abs(x) > 5, expected_jitter)

        parse_pcap.PcapParser.scatterplot(
            expected_jitter,
            xrange(len(expected_jitter)),
            file_name='./out/expected_jitter.png')


    def delay(self):
        try:
            duration = self.packet_config.popleft()
        except IndexError, e:
            self.packet_config = Simulation.load_packet_config(self.net_config)
            duration = self.packet_config.popleft()

        if duration > 20:
            duration = duration - 20
            time.sleep(duration / 1000.0)
        # print 'slept', duration

    def modify(self, packet):
        self.delay()
        packet.accept()

    def start(self):
        self.nfqueue = NetfilterQueue()
        self.nfqueue.bind(11, self.modify)
        try:
            print "[*] waiting for data"
            self.nfqueue.run()
            print 'done'
        except KeyboardInterrupt:
            print 'user exits'
        except Exception, e:
            print 'exiting due to', e

    def stop(self):
        self.nfqueue.unbind()

if __name__ == '__main__':
    j = Simulation()
    j.start()
