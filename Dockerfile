FROM ubuntu:16.04
RUN DEBIAN_FRONTEND='noninteractive' apt-get update
RUN apt-get install -y libx11-6 pulseaudio tcpdump python vim
RUN apt-get install -y alsa-utils

RUN mkdir /jitter
RUN touch /jitter/voe.log
RUN ln -sf /jitter/voe.log /dev/stdout
RUN ln -sf /jitter/voe_err.log /dev/stderr

ADD . /jitter
RUN mv /usr/sbin/tcpdump /usr/bin/tcpdump

WORKDIR /jitter
CMD ./voe_cmd_test
