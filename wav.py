import wave
import contextlib
import os

def convert_to_wav(pcm):
    with open(pcm, 'rb') as pcm_file:
        pcmdata = pcm_file.read()
    with contextlib.closing(wave.open(pcm + '.wav', 'wb')) as wavfile:
        wavfile.setparams((1, 2, 16000, 0, 'NONE', 'NONE'))
        wavfile.writeframes(pcmdata)

    os.system('sox ' + pcm + '.wav ' + pcm +'_1.wav silence 1 1 1.5% reverse silence 1 1 0.5% reverse')
    os.system('mv ' + pcm + '_1.wav ' + pcm + '.wav')

def parse_outputs():
    convert_to_wav('./out/recorded_playout.pcm')
    os.system('cp recorded_mic.pcm ./out/recorded_mic.pcm')
    convert_to_wav('./out/recorded_mic.pcm')

