import subprocess
import os
import time
import wav 
import parse_pcap2 as parse_pcap
import nfq
import threading
import sys
from multiprocessing import Process
import waveform

def call(command):
    os.system(command)

def stop_endpoints():
    command = "sudo docker stop $(sudo docker ps -a -q) && sudo docker rm $(sudo docker ps -a -q)"
    call(command)

def start_endpoints(hybrid=True):
    call("sudo docker run -t --rm --privileged --name=sender --entrypoint=./sender.sh webrtc &")
    time.sleep(1)
    if hybrid:
        call("sudo docker run -t --rm --privileged --name=recv --entrypoint=./recv.sh webrtc &")
    else:
        call("sudo docker run -t --rm --privileged --name=recv --entrypoint=./recv_no_hybrid.sh webrtc &")
def tcpdump():
    print '********************start tcpdump********************'
    call("sudo docker exec recv tcpdump -i any -w tcpdump.dmp &")
    call("sudo docker exec sender tcpdump -i any -w tcpdump.dmp &")

def collect_data():
    command = "sudo docker cp recv:/jitter/recorded_playout.pcm ./out"
    call(command)
    
    command = "sudo docker cp recv:/jitter/tcpdump.dmp ./out/tcpdump_recv.dmp"
    call(command)

    command = "sudo docker cp sender:/jitter/tcpdump.dmp ./out/tcpdump_sender.dmp"
    call(command)

    command = "sudo docker cp recv:/jitter/run.log ./out"
    call(command)

    command = "sudo docker cp recv:/jitter/recording ./out"
    call(command)

    call("docker cp recv:/jitter/webrtc_trace.txt ./out")

sim = None
sim_process = None
def start_sim():
    def start_sim_worker():
        sim.start()

    global sim
    global sim_process
    sim = nfq.Simulation(net_config_file)
    sim_process = Process(target=start_sim_worker)
    sim_process.start()

# replace with polqa or another model if need be
def pesq():
    command = './pesq +16000 ./out/recorded_mic.pcm ./out/recorded_playout.pcm | grep Prediction > ./out/pesq.txt'
    call(command)

def clean():
    call('mkdir -p ./out')
    call('rm -rf ./out/*')
    command = 'rm -rf ~/Dropbox/tests/*'
    call(command)

def cp_dropbox():
    print 'copy to dropbox'
    command = 'cp -r ./out_* ~/Dropbox/tests/'
    call(command)

def main(hybrid=True):
    clean()
    start_sim()
    start_endpoints(hybrid)
    time.sleep(1)
    tcpdump()
    time.sleep(10)
    collect_data()
    stop_endpoints()
    wav.parse_outputs()
    waveform.show_wave_n_spec(hybrid)
    # pesq()
    parse_pcap.PcapParser(net_config_file).run()
    sim_process.join(1)
    sim_process.terminate()

net_config_file=None
def run():
    global net_config_file
    net_config_file = './profiles/moha.19'
    call('rm -rf ./out_hybrid')
    call('rm -rf ./out_no_hybrid')

    main(hybrid=True)
    call('mv ./out ./out_hybrid')
    main(hybrid=False)
    call('mv ./out ./out_no_hybrid')
    cp_dropbox()

if __name__ == '__main__':
    run()
